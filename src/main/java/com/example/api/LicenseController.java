package com.example.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class LicenseController {

	@GetMapping("/license/{id}")
	public License getLicense(@PathVariable long id) {
		String name = "Example license";
		Date queriedAt = new Date();
		return new License(id, name, queriedAt);
	}
}