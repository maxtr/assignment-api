package com.example.api;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class License {

	private final long id;
	private final String name;
	@JsonFormat(pattern="yyyy-MM-dd")
	private final Date queriedAt;

	public License(long id, String name, Date queriedAt) {
		this.id = id;
		this.name = name;
		this.queriedAt = queriedAt;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Date getQueriedAt() {
		return queriedAt;
	}
}
