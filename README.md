# README #

This is a simple REST API implemented with Spring Boot.

### Instructions ###

* Clone the repository
* In the project root folder, run `./gradlew bootRun`
* Access the api at http://localhost:8080/api/license/{id}
      * example: [http://localhost:8080/api/license/5](http://localhost:8080/api/license/5)